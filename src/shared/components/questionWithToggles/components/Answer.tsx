import { CSSProperties, Dispatch, SetStateAction } from "react";
import {
	AnswerType,
	SelectedAnswer,
	ToggleType
} from "shared/types/questionWithToggles";
import styles from "../styles/answer.module.scss";

interface AnswerProps {
	toggle: ToggleType;
	answer: AnswerType;
	selectedAnswers: SelectedAnswer[];
	setSelectedAnswers: Dispatch<SetStateAction<SelectedAnswer[] | undefined>>;
	areAllAnswersCorrect: () => boolean;
}

export const Answer = (props: AnswerProps) => {
	const {
		toggle,
		answer,
		selectedAnswers,
		setSelectedAnswers,
		areAllAnswersCorrect
	} = props;

	const handleAnswerClick = (
		toggle: ToggleType,
		answer: AnswerType
	): void => {
		if (!selectedAnswers || areAllAnswersCorrect()) return;
		const oldSelectedAnswer = selectedAnswers.find(
			(selectedAnswer) => selectedAnswer.toggleId === toggle.id
		);
		if (!oldSelectedAnswer) return;
		if (oldSelectedAnswer.answerId !== answer.id) {
			setSelectedAnswers(
				selectedAnswers.map((selectedAnswer) => {
					if (selectedAnswer.toggleId === toggle.id)
						return { toggleId: toggle.id, answerId: answer.id };
					return selectedAnswer;
				})
			);
		}
	};

	const getAnswerTextStyle = (
		toggle: ToggleType,
		answer: AnswerType
	): CSSProperties | undefined => {
		if (!selectedAnswers) return;
		const selectedAnswer = selectedAnswers.find(
			(selectedAnswer) => selectedAnswer.toggleId === toggle.id
		);
		if (!selectedAnswer) return;
		if (answer.id === selectedAnswer.answerId) return { color: "#9f938b" };
		return;
	};

	return (
		<div
			className={styles.answer}
			key={answer.id}
			onClick={() => handleAnswerClick(toggle, answer)}
		>
			<p style={getAnswerTextStyle(toggle, answer)}>{answer.text}</p>
		</div>
	);
};
