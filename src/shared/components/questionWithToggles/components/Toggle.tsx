import { CSSProperties, Dispatch, SetStateAction } from "react";
import { BREAKPOINTS, MAX_LENGTH_OF_ANSWER } from "shared/constants";
import {
	AnswerType,
	SelectedAnswer,
	ToggleType
} from "shared/types/questionWithToggles";
import { useWindowWidth } from "../hooks";
import toggleStyles from "../styles/toggle.module.scss";
import selectedAnswerStyles from "../styles/selected-answer.module.scss";
import { Answer } from "./Answer";

interface ToggleProps {
	toggle: ToggleType;
	selectedAnswers: SelectedAnswer[];
	setSelectedAnswers: Dispatch<SetStateAction<SelectedAnswer[] | undefined>>;
	areAllAnswersCorrect: () => boolean;
}

export const Toggle = (props: ToggleProps) => {
	const {
		toggle,
		selectedAnswers,
		setSelectedAnswers,
		areAllAnswersCorrect
	} = props;
	const windowWidth = useWindowWidth();

	const isSomeAnswerToLong = (maxLength: number): boolean => {
		return toggle.answers.some(
			(answer: AnswerType) => answer.text.length > maxLength
		);
	};

	// Used to determine if the answers in toggle are to be placed in a row or in a column
	const areAnswersInColumn = (): boolean => {
		if (windowWidth <= BREAKPOINTS.SMALL) {
			if (toggle.answers.length > 3) return true;
			return isSomeAnswerToLong(
				MAX_LENGTH_OF_ANSWER.ON_EXTRA_SMALL[
					toggle.answers.length.toString()
				]
			);
		}
		if (
			windowWidth > BREAKPOINTS.SMALL &&
			windowWidth <= BREAKPOINTS.MEDIUM
		) {
			if (toggle.answers.length > 4) return true;
			return isSomeAnswerToLong(
				MAX_LENGTH_OF_ANSWER.ON_SMALL[toggle.answers.length.toString()]
			);
		}
		if (
			windowWidth > BREAKPOINTS.MEDIUM &&
			windowWidth <= BREAKPOINTS.LARGE
		) {
			if (toggle.answers.length > 5) return true;
			return isSomeAnswerToLong(
				MAX_LENGTH_OF_ANSWER.ON_MEDIUM[toggle.answers.length.toString()]
			);
		}
		if (windowWidth > BREAKPOINTS.LARGE) {
			if (toggle.answers.length > 5) return true;
			return isSomeAnswerToLong(
				MAX_LENGTH_OF_ANSWER.ON_LARGE[toggle.answers.length.toString()]
			);
		}
		return false;
	};

	const getSelectedAnswerIndex = (): number => {
		const selectedAnswer = selectedAnswers?.find(
			(selectedAnswer: SelectedAnswer) =>
				selectedAnswer.toggleId === toggle.id
		);
		return toggle.answers.findIndex(
			(answer: AnswerType) => answer.id === selectedAnswer?.answerId
		);
	};

	const getToggleContainerClassName = (): string => {
		let className: string = toggleStyles["toggle-container"];

		if (areAnswersInColumn()) {
			className = `${className} ${toggleStyles["toggle-container-column"]}`;
		}
		return className;
	};

	const getSelectedAnswerClassName = (): string => {
		let className: string = selectedAnswerStyles["selected-answer"];

		if (areAnswersInColumn()) {
			className = `${className} ${selectedAnswerStyles["selected-answer-column"]}`;

			const numberOfAnswers = toggle.answers.length;
			const selectedAnswerIndex = getSelectedAnswerIndex();
			if (selectedAnswerIndex === 0) {
				className = `${className} ${selectedAnswerStyles["selected-answer-first"]}`;
			}
			if (
				selectedAnswerIndex > 0 &&
				selectedAnswerIndex < numberOfAnswers - 1
			) {
				className = `${className} ${selectedAnswerStyles["selected-answer-middle"]}`;
			}
			if (selectedAnswerIndex === numberOfAnswers - 1) {
				className = `${className} ${selectedAnswerStyles["selected-answer-last"]}`;
			}
		}
		return className;
	};

	// Calculate selected answer width/height and position
	const getSelectedAnswerStyle = (): CSSProperties | undefined => {
		if (!selectedAnswers) return;
		const numberOfAnswers = toggle.answers.length;
		const selectedAnswerIndex = getSelectedAnswerIndex();

		if (areAnswersInColumn()) {
			const height = `${(1 / numberOfAnswers) * 100}%`;
			const top = `${(selectedAnswerIndex / numberOfAnswers) * 100}%`;
			return { height: height, top: top };
		}

		const width = `${(1 / numberOfAnswers) * 100}%`;
		const left = `${(selectedAnswerIndex / numberOfAnswers) * 100}%`;
		return { width: width, left: left };
	};

	return (
		<div className={getToggleContainerClassName()} key={toggle.id}>
			{toggle.answers.map((answer: AnswerType) => (
				<Answer
					key={answer.id}
					toggle={toggle}
					answer={answer}
					selectedAnswers={selectedAnswers}
					setSelectedAnswers={setSelectedAnswers}
					areAllAnswersCorrect={areAllAnswersCorrect}
				/>
			))}
			<div
				className={getSelectedAnswerClassName()}
				style={getSelectedAnswerStyle()}
			/>
		</div>
	);
};
