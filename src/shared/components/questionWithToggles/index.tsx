import { CSSProperties, useEffect, useState } from "react";
import {
	AnswerType,
	QuestionWithTogglesType,
	SelectedAnswer,
	ToggleType
} from "shared/types/questionWithToggles";
import { Toggle } from "./components";
import {
	useRandomiseToggleAndAnswerPositions,
	useInitializeSelectedAnswers
} from "./hooks";
import styles from "./styles/question-with-toggles.module.scss";

interface QuestionWithTogglesProps {
	questionWithToggles: QuestionWithTogglesType;
}

export const QuestionWithToggles = (props: QuestionWithTogglesProps) => {
	const { questionWithToggles } = props;
	const [selectedAnswers, setSelectedAnswers] = useState<SelectedAnswer[]>();

	useEffect(() => {
		useRandomiseToggleAndAnswerPositions(questionWithToggles);
		useInitializeSelectedAnswers(questionWithToggles, setSelectedAnswers);
	}, [questionWithToggles]);

	const getPercentageOfCorrectAnswers = (): number => {
		const correctAnswers = questionWithToggles.toggles.map(
			(toggle: ToggleType) => {
				return {
					toggleId: toggle.id,
					answerId: toggle.answers.find(
						(answer: AnswerType) => answer.isCorrect
					)?.id
				};
			}
		);
		let numberOfCorrectSelectedAnswers = 0;
		selectedAnswers?.forEach((selectedAnswer: SelectedAnswer) => {
			const correctAnswer = correctAnswers.find(
				(correctAnswer) =>
					correctAnswer.toggleId === selectedAnswer.toggleId
			);
			if (selectedAnswer.answerId === correctAnswer?.answerId) {
				numberOfCorrectSelectedAnswers++;
			}
		});
		return (
			numberOfCorrectSelectedAnswers / questionWithToggles.toggles.length
		);
	};

	// Changes the background based on the number of correct answers
	const getQuestionWithTogglesContainerStyle = (): CSSProperties => {
		const percentageOfCorrectAnswers = getPercentageOfCorrectAnswers();
		let hueLighter: number;
		let hueDarker: number;
		let background: string;
		if (percentageOfCorrectAnswers === 1) {
			background = "linear-gradient(#4caf50, #009688)";
		} else {
			hueLighter = 22 + percentageOfCorrectAnswers * 30;
			hueDarker = 12 + percentageOfCorrectAnswers * 30;
			background = `linear-gradient(hsl(${hueLighter}, 94%, 64%), hsl(${hueDarker}, 94%, 56%))`;
		}
		return { background: background };
	};

	const areAllAnswersCorrect = (): boolean => {
		const percentageOfCorrectAnswers = getPercentageOfCorrectAnswers();
		return percentageOfCorrectAnswers === 1;
	};

	if (!selectedAnswers) return null;

	return (
		<div
			className={styles["question-with-toggles-container"]}
			style={getQuestionWithTogglesContainerStyle()}
		>
			<div className={styles["question-text"]}>
				{questionWithToggles.text}
			</div>
			<div className={styles["toggles-container"]}>
				{questionWithToggles.toggles.map((toggle: ToggleType) => (
					<Toggle
						key={toggle.id}
						toggle={toggle}
						selectedAnswers={selectedAnswers}
						setSelectedAnswers={setSelectedAnswers}
						areAllAnswersCorrect={areAllAnswersCorrect}
					/>
				))}
			</div>
			<div className={styles.result}>{`The answer is ${
				areAllAnswersCorrect() ? "correct" : "incorrect"
			}`}</div>
		</div>
	);
};
