import { Dispatch, SetStateAction } from "react";
import {
	QuestionWithTogglesType,
	SelectedAnswer,
	ToggleType
} from "shared/types/questionWithToggles";
import { getRandomItemFromArray } from "shared/utils";

// Set initially selected toggle answers
// (50% incorrect and others random to avoid situation
// when all toggles are initially set to correct answer)
export const useInitializeSelectedAnswers = (
	questionWithToggles: QuestionWithTogglesType,
	setSelectedAnswers: Dispatch<SetStateAction<SelectedAnswer[] | undefined>>
) => {
	const initiallySelectedAnswers: SelectedAnswer[] = [];
	let toggles = questionWithToggles.toggles;

	// Set initially selected answer of 50% of toggles to incorrect answer
	const numberOfIncorrectlySetToggles = Math.ceil(toggles.length / 2);
	for (let i = 0; i < numberOfIncorrectlySetToggles; i++) {
		const randomToggle = getRandomItemFromArray(toggles);
		const randomNegativeAnswer = getRandomItemFromArray(
			randomToggle.answers.filter((answer) => !answer.isCorrect)
		);
		initiallySelectedAnswers.push({
			toggleId: randomToggle.id,
			answerId: randomNegativeAnswer.id
		});
		toggles = toggles.filter((toggle) => toggle.id !== randomToggle.id);
	}

	// Set initially selected answer of other toggles to random answer
	toggles.forEach((toggle: ToggleType) => {
		initiallySelectedAnswers.push({
			toggleId: toggle.id,
			answerId: getRandomItemFromArray(toggle.answers).id
		});
	});

	setSelectedAnswers(initiallySelectedAnswers);
};
