import {
	QuestionWithTogglesType,
	ToggleType
} from "shared/types/questionWithToggles";
import { shuffleArray } from "shared/utils";

export const useRandomiseToggleAndAnswerPositions = (
	questionWithToggles: QuestionWithTogglesType
) => {
	shuffleArray(questionWithToggles.toggles);
	questionWithToggles.toggles.forEach((toggle: ToggleType) =>
		shuffleArray(toggle.answers)
	);
};
