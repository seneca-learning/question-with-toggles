export interface QuestionWithTogglesType {
	id: number;
	text: string;
	toggles: ToggleType[];
}

export interface ToggleType {
	id: number;
	answers: AnswerType[];
}

export interface AnswerType {
	id: number;
	text: string;
	isCorrect: boolean;
}

export interface SelectedAnswer {
	toggleId: number;
	answerId: number;
}
