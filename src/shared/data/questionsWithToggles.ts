import { QuestionWithTogglesType } from "shared/types/questionWithToggles";

export const questionsWithToggles: QuestionWithTogglesType[] = [
	{
		id: 1,
		text: "An animal cell contains:",
		toggles: [
			{
				id: 1,
				answers: [
					{ id: 1, text: "Ribosomes", isCorrect: true },
					{ id: 2, text: "Cell wall", isCorrect: false }
				]
			},
			{
				id: 2,
				answers: [
					{ id: 3, text: "Cytoplasm", isCorrect: true },
					{ id: 4, text: "Chloroplast", isCorrect: false }
				]
			},
			{
				id: 3,
				answers: [
					{
						id: 5,
						text: "Partially permeable membrane",
						isCorrect: true
					},
					{ id: 6, text: "Impermeable membrane", isCorrect: false }
				]
			},
			{
				id: 4,
				answers: [
					{ id: 7, text: "Mitochondira", isCorrect: true },
					{ id: 8, text: "Cellulose", isCorrect: false }
				]
			}
		]
	},
	{
		id: 2,
		text: "What are the ideal conditions inside an office?",
		toggles: [
			{
				id: 5,
				answers: [
					{ id: 9, text: "Good pay", isCorrect: true },
					{ id: 10, text: "Bad pay", isCorrect: false }
				]
			},
			{
				id: 6,
				answers: [
					{ id: 11, text: "Less meetings", isCorrect: true },
					{ id: 12, text: "Lot of meetings", isCorrect: false }
				]
			},
			{
				id: 7,
				answers: [
					{
						id: 13,
						text: "Free coffee",
						isCorrect: true
					},
					{ id: 14, text: "Expensive coffee", isCorrect: false }
				]
			},
			{
				id: 8,
				answers: [
					{ id: 15, text: "Dog in office", isCorrect: true },
					{ id: 16, text: "Bear in office", isCorrect: false }
				]
			}
		]
	},
	{
		id: 3,
		text: "Which are the best sports people & teams?",
		toggles: [
			{
				id: 9,
				answers: [
					{ id: 17, text: "Liverpool", isCorrect: true },
					{ id: 18, text: "Chelsea", isCorrect: false },
					{ id: 19, text: "Man Utd", isCorrect: false }
				]
			},
			{
				id: 10,
				answers: [
					{ id: 20, text: "Serena Williams", isCorrect: true },
					{ id: 21, text: "Naomi Osaka", isCorrect: false }
				]
			}
		]
	},
	{
		id: 4,
		text: "Which city is the capital in its country?",
		toggles: [
			{
				id: 11,
				answers: [
					{ id: 22, text: "Zagreb", isCorrect: true },
					{ id: 23, text: "Zadar", isCorrect: false }
				]
			},
			{
				id: 12,
				answers: [
					{ id: 24, text: "London", isCorrect: true },
					{ id: 25, text: "Manchester", isCorrect: false },
					{ id: 26, text: "Birmingham", isCorrect: false }
				]
			},
			{
				id: 13,
				answers: [
					{ id: 27, text: "Berlin", isCorrect: true },
					{ id: 28, text: "Munich", isCorrect: false },
					{ id: 29, text: "Frankfurt", isCorrect: false },
					{ id: 30, text: "Hamburg", isCorrect: false }
				]
			},
			{
				id: 14,
				answers: [
					{ id: 31, text: "Washington", isCorrect: true },
					{ id: 32, text: "New York", isCorrect: false },
					{ id: 33, text: "Los Angeles", isCorrect: false },
					{ id: 34, text: "Chicago", isCorrect: false }
				]
			},
			{
				id: 15,
				answers: [
					{ id: 35, text: "Bern", isCorrect: true },
					{ id: 36, text: "Zurich", isCorrect: false },
					{ id: 37, text: "Geneva", isCorrect: false }
				]
			}
		]
	},
	{
		id: 5,
		text: "Which of these is the last book/film in its series?",
		toggles: [
			{
				id: 16,
				answers: [
					{
						id: 38,
						text: "The Lord of the Rings: The Return of the King",
						isCorrect: true
					},
					{
						id: 39,
						text: "The Lord of the Rings: The Two Towers",
						isCorrect: false
					},
					{
						id: 40,
						text: "The Lord of the Rings: The Fellowship of the Ring",
						isCorrect: false
					}
				]
			},
			{
				id: 17,
				answers: [
					{
						id: 41,
						text: "Harry Potter and the Deathly Hallows",
						isCorrect: true
					},
					{
						id: 42,
						text: "Harry Potter and the Half-Blood Prince",
						isCorrect: false
					},
					{
						id: 43,
						text: "Harry Potter and the Order of the Phoenix",
						isCorrect: false
					},
					{
						id: 44,
						text: "Harry Potter and the Goblet of Fire",
						isCorrect: false
					}
				]
			}
		]
	}
];
