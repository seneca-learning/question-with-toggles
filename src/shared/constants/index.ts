export const BREAKPOINTS = {
	SMALL: 480,
	MEDIUM: 800,
	LARGE: 1280
};

// Key is the number of answers in a toggle.
// Value represents the number of characters.
export const MAX_LENGTH_OF_ANSWER: {
	[breakpoint: string]: { [length: string]: number };
} = {
	ON_EXTRA_SMALL: { "2": 16, "3": 8 },
	ON_SMALL: { "2": 20, "3": 12, "4": 8 },
	ON_MEDIUM: { "2": 48, "3": 24, "4": 16, "5": 8 },
	ON_LARGE: { "2": 64, "3": 32, "4": 24, "5": 16 }
};
