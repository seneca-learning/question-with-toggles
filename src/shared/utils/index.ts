// Implementation of the Durstenfeld shuffle (the modern version of the Fisher–Yates shuffle)
// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
export const shuffleArray = <Type>(array: Type[]) => {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
};

export const getRandomItemFromArray = <Type>(array: Type[]): Type => {
	return array[Math.floor(Math.random() * array.length)];
};
