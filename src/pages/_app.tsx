import type { AppProps } from "next/app";
import Head from "next/head";
import { Fragment } from "react";
import "shared/styles/index.scss";

const App = ({ Component, pageProps }: AppProps) => {
	return (
		<Fragment>
			<Head>
				<title>Question with Toggles</title>
			</Head>
			<Component {...pageProps} />;
		</Fragment>
	);
};

export default App;
