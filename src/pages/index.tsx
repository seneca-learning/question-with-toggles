import type { NextPage } from "next";
import { useEffect, useState } from "react";
import styles from "modules/home/home.module.scss";
import { QuestionWithToggles } from "shared/components/questionWithToggles";
import { questionsWithToggles } from "shared/data/questionsWithToggles";
import { QuestionWithTogglesType } from "shared/types/questionWithToggles";
import { getRandomItemFromArray } from "shared/utils";

const Home: NextPage = () => {
	const [questionWithToggles, setQuestionWithToggles] =
		useState<QuestionWithTogglesType>();

	// Get a random question when page is first opened
	useEffect(() => {
		setQuestionWithToggles(getRandomItemFromArray(questionsWithToggles));
	}, []);

	// Get a new question different from the last one
	const getNewQuestion = (): void => {
		const oldQuestionWithToggles = questionWithToggles;
		let newQuestionWithToggles: QuestionWithTogglesType;
		do {
			newQuestionWithToggles =
				getRandomItemFromArray(questionsWithToggles);
		} while (newQuestionWithToggles.id === oldQuestionWithToggles?.id);
		setQuestionWithToggles(newQuestionWithToggles);
	};

	return (
		<main className={styles.home}>
			{questionWithToggles && (
				<QuestionWithToggles
					questionWithToggles={questionWithToggles}
				/>
			)}

			<button
				className={styles["new-question-button"]}
				onClick={getNewQuestion}
			>
				New question
			</button>
		</main>
	);
};

export default Home;
