This is a [Next.js](https://nextjs.org/) project bootstrapped
with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Starting the app

First run:

```bash
npm install
```

Then run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000)

## Packages used

- React with Next.js
- Type checking: TypeScript
- Style sheets: Sass
- Code formatting: Prettier

## System requirements

- Node.js version 12.22 or later

## Solution description, assumptions and limitations

- The *QuestionWithToggles* component can accommodate a question with any number of toggles (answer groups).
- A *Toggle* can have any number of positions (answers); 2, 3, 4,..., although more than 4 is not recommended (in that
  case a select component (dropdown, listbox) might be more suitable).
- The *QuestionWithToggles* component receives a question object with the following data structure assumed:
    - *Question*
        - *id*: number - question id
        - *text*: string - question text
        - *toggles*: *Toggle[]* - array of toggles (answer groups)
    - *Toggle*
        - *id*: number - toggle id
        - *answers*: *Answer[]* - array of answers
    - *Answer*
        - *id*: number - answer id
        - *text*: string - answer text
        - *isCorrect*: boolean - is the answers correct
- It is assumed that the data received by *QuestionWithToggles* component is correct and valid.
- The question data is stored in *src/shared/data/questionsWithToggles.ts*. New questions can be added there.
- The component is responsive. Layout of answers in a toggle depends on window (screen) size and the length of answer
  texts. Answers in a toggle are placed in a row layout by default, but if some answer is too long the answers will be
  placed in a column layout.
- For randomising the order of toggles and answer positions in a toggle, an implementation of the Durstenfeld shuffle (
  the modern version of the Fisher–Yates shuffle) is used.
- Switching between questions is possible by clicking on the "*New question*" button located below the question
  component.

### Project folder structure

- *QuestionWithToggles* component is called from Home page, which is in *src/pages/index.tsx*.
- *QuestionWithToggles* component is in *src/shared/components/questionWithToggles/index.tsx*.
- Parts of *QuestionWithToggles* component are in *components*, *hooks* and *styles* folders which are located in 
  *src/shared/components/questionWithToggles* folder.
- Other constants, styles, types and utils used are in their respective folders which are located in *src/shared*
  folder.

---

## Task

### Outline

The task is to make a component using React. A video of how the component should work is attached.

The link to the figma project is here which has designs that can help you:
https://www.figma.com/file/Ee8fqEKdF5ZXpmCGflCdMj/Seneca-tech-task?node-id=0%3A1

**TIP** If you haven't used figma before. Checkout the "code" section inside the "inspect" tab on the right hand side of
the screen to see CSS properties:
https://help.figma.com/hc/en-us/articles/360055203533-Use-the-Inspect-panel#View_code

### Requirements, please:

- State any assumptions or limitations of your solution in the repository readme
- Put your solution in git repo & email the link once you are done
- Some form of type checking should be used e.g. flow, propTypes, typescript
- The component should be responsive down to 320px
- The solution should lock once the correct answer is reached so the toggles can no longer be switched
- Ignore the navbar or footer just the toggles component itself
- The toggles should animate between the two states (see attached video)
- The background color should change in proportion to how "correct" the answer is (see video attached)
- The component should be reusable & extendable, it should be able to accommodate the question changing from that in the
  video to e.g:

Q. "What are the ideal conditions inside an office?"
A. (good pay, bad pay) (lot of meetings, less meetings), (free coffee, expensive coffee), (bear in office, dog in
office).

Extension:

- The order of the questions & answer positions should be randomised
- Your solution should be able to accommodate answers with both two and three toggle positions in the answers. For
  example:
  Q. "Which are the best sports people & teams?"
  A. (Liverpool, Chelsea, Man Utd), (Serena Williams, Naomi Osaka)
- You should make it easy to switch between the active question
